import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Text, ToastAndroid} from 'react-native';
import { BleManager } from 'react-native-ble-plx';
import base64 from 'react-native-base64'
import {Buffer} from 'buffer';
// import { parse } from 'fast-xml-parser';
import { parseString } from 'react-native-xml2js'


// IMPORT Command
import { Runsys, Command, Mesures } from '../Devices'



// Calcule pour la valeur final bio impedance:
// =SI(E5>8388608;ARRONDI((E5-8388608*2)*0,005960464477538/16;2);ARRONDI(E5*0,005960464477538/16;2))

// Variable global
const blemanager = new BleManager()

const Page2 = () => {

    const [urlForRunsysServer, setUrlForRunsysServer] = useState();
    // const [xmlResponse, setXmlresponse] = useState()

     var megaTab = []
     var finalBiomeasur2 = []
     var url = ""
     var Impedance = []

     const [lastvalueImpedance, setValueImpedance]  = useState();

    useEffect(() => {
          const subscription = blemanager.onStateChange((state) => {
              if (state === 'PoweredOn') {
                scanAndConnectRunsys();
                  subscription.remove();
              }else if(state ==='PoweredOff'){
                blemanager.enable()
                console.log("le bluetooth est activé ");
              }
          }, true);
          
          return () => subscription.remove();
         
      }, []);


    const showToastCoonectedToRunsy = () => {
        ToastAndroid.show("Connexion à Runsys Réussi", ToastAndroid.SHORT);
      };
    const scanAndConnectRunsys = () => {
        console.log("recherche dappareil")
        blemanager.startDeviceScan(null, null, (error, device) => {

            if (error) {
                // Handle error (scanning will be stopped automatically)
                return
            }

            if (device.name === Runsys.name || device.id === Runsys.id) {
                // Stop scanning as it's not necessary if you are scanning for one device.
                blemanager.stopDeviceScan();
                showToastCoonectedToRunsy()
                // console.log("Connexion reussi");
                
                device.connect()
                .then((deviceConnected) => {
                    console.log("nous somme connecté à: ", deviceConnected.name)
                    deviceConnected.discoverAllServicesAndCharacteristics()
                    .then(() => {
                        blemanager.servicesForDevice(deviceConnected.id)
                        .then((services) => {
                            console.log("Les services de: ",  deviceConnected.name, "\n", services)
                            
                            console.log("On rentre dans la boucle \n");
                            // boucle pour envoyer toutes les commandes

                            var count = 0
                            const ReadCmd = () => {
                                for (var i = 0; i < Command.length; i++) {
                                    deviceConnected.writeCharacteristicWithResponseForService(
                                        "36794f20-3a88-418c-8df8-7394c5c80200",
                                        "36794f20-3a88-418c-8df8-7394c5c80201",
                                        base64.encodeFromByteArray(Uint8Array.from(Buffer.from(Command[i].value, 'hex'))),
                                    ).then((characteristic) => {
                                        console.log("Commande ecrite: ", i, characteristic.value)
                                    })
                                    count += 1
                                    if (count === 3) {
                                        
                                        clearInterval(intervalReadCmd)
                                        console.log("STOP ENVOI DE COMMANDES");
                                    }
                                        
                                    // on ecrit toutes les valeur
                                    
                                // Fin de la boucle des commandes
                                }
                            }
                            let intervalReadCmd = setInterval(ReadCmd,  1*1000)

                                setTimeout(() => {
                                    for (var i = 0; i < Mesures.length; i++) {

                                        // console.log("Mesure écrite: ", Mesures[i].name);
                                                deviceConnected.writeCharacteristicWithResponseForService(
                                                    "36794f20-3a88-418c-8df8-7394c5c80200",
                                                    "36794f20-3a88-418c-8df8-7394c5c80201",
                                                    base64.encodeFromByteArray(Uint8Array.from(Buffer.from(Mesures[i].value, 'hex'))),
                                                ).then ((write) => {
                                                    const readValue = Buffer.from( write.value, 'base64').toString('hex');
                                                    // console.log("Valeur écrite en Hexa: ", readValue)
                                                });
        
                                                deviceConnected.readCharacteristicForService(
                                                    "e9795783-3ce2-4009-aa2b-818de4c40100",
                                                    "e9795783-3ce2-4009-aa2b-818de4c40101",
                                                ).then((value) => {
                                                    let bioValue = Buffer.from(value.value, 'base64').toString('hex');
                                                    console.log("Valeur Reçu en hexa: => ", bioValue)
                                                    // addBioimpedanceList(bioValue)
                                                    megaTab.push(bioValue)
                                                    console.log("Mega Table =>", megaTab.length)
                                                    // console.log("taille du tableau bioImpedanceData: "+ bioImpedanceData.length)
                                                    if (megaTab.length ===  12) {
                                                        console.log("Mega Table: \n ", megaTab)
                                                        extratctMeasureFromBioImpedanceList()
                                                    }else {
                                                        console.log("Tableau vide...")
                                                    }
                                                })
                                        // FIN Boucle pour envoie des mesures 
                                    }
                                    
                                }, 1 * 3000);
                                
                        })
                    })
                    
                })
                
            }
            
        });
       
    } 

    const extratctMeasureFromBioImpedanceList = () => {
        megaTab.map((measure) => {
        extractMeasureFromBioImpedanceMesur(measure)
        console.log("EXTRACTION:::")
    })
    }

    const addFinalBiomeasur = (finalBio) => {
        console.log("ajout dans le tableau de la finalBio: ", finalBio);
        setFinalBioimpedancedata(prevFinalbiolist => [...prevFinalbiolist, {
              id: prevFinalbiolist.length,
              value: finalBio
            }]);
      }

    const extractMeasureFromBioImpedanceMesur = (biomeasur) => {
        let measureBioFinal;
        console.log("Extraction de la mesure: ", biomeasur)
        const measureBioImpedance = (biomeasur.slice(8, 14));
        console.log("Valeur extraite: ",measureBioImpedance);
        let measurBioDecimal = parseInt(measureBioImpedance, 16)
        console.log("Valeur en décimal =>: ", measurBioDecimal)

            if( measurBioDecimal > 8388608) {
                measurBioDecimal = measurBioDecimal - 2 * 8388608
                measurBioDecimal = measurBioDecimal * 0.005960464477538 /16
                measureBioFinal = measurBioDecimal;
                console.log("bio final:", measureBioFinal.toFixed(2))
                finalBiomeasur2.push(measureBioFinal.toFixed(2))
                // addFinalBiomeasur(measureBioFinal.toFixed(2))
            }else {
                measurBioDecimal = measurBioDecimal * 0.005960464477538 /16
                measureBioFinal = measurBioDecimal
                console.log("Bio final: ", measureBioFinal.toFixed(2))
                finalBiomeasur2.push(measureBioFinal.toFixed(2))
                // addFinalBiomeasur(measureBioFinal.toFixed(2))
            }


            console.log("Final bio mesure2: ",finalBiomeasur2)
            // console.log("Valeur du tableau final: \n",finalBioimpedancedata)
            const baseUrl = "https://apiz.space/HomeHabilis/APIRawV12.php?Serial=1aZ8&Weight=80.5&Height=176&Age=32&Sex=1&Code=1234&Comment=mqsodkmqsdklkmlqdk&Category=1&Electrode=0&M4kHz0deg="
            const baseUrl2 = "&M4kHz90deg="
            const baseUrl3 = "&M8kHz0deg="
            const baseUrl4 = "&M8kHz90deg="
            const baseUrl5 = "&M18kHz0deg="
            const baseUrl6 = "&M18kHz90deg="
            const baseUrl7 = "&M40kHz0deg="
            const baseUrl8 = "&M40kHz90deg="
            const baseUrl9 = "&M80kHz0deg="
            const baseUrl10 = "&M80kHz90deg="
            const baseUrl11 = "&M128kHz0deg="
            const baseUrl12 = "&M128kHz90deg="
           
            let urlExtract = baseUrl+finalBiomeasur2[0]+baseUrl2+finalBiomeasur2[1]+baseUrl3+finalBiomeasur2[2]+baseUrl4+finalBiomeasur2[3]+baseUrl5+finalBiomeasur2[4]+baseUrl6+finalBiomeasur2[5]+baseUrl7+finalBiomeasur2[6]+baseUrl8+finalBiomeasur2[7]+baseUrl9+finalBiomeasur2[8]+baseUrl10+finalBiomeasur2[9]+baseUrl11+finalBiomeasur2[10]+baseUrl12+finalBiomeasur2[11]
            console.log("L'URL GENERE: \n", urlExtract.toString())

            url = urlExtract;
            setUrlForRunsysServer(url.toString())
            console.log("Url pour le server :=>" + url);
    }
                

        const getApi = () => {

            fetch(urlForRunsysServer)
                .then((response) => response.text())
                .then((textResponse) => {
                    // console.log('response is ', textResponse)
                    // setXmlresponse(textResponse)

                    parseString(textResponse, function (err, result) {
                        console.log("xml to json \n",result.Indices);
                        let Indices = result.Indices
                        console.log("Les Indices: ",Indices)
                        Indices.indice.map((indice) =>{
                                console.log(indice.name+indice.value);
                                // data.push(indice.name)
                                Impedance.push(indice.value)
                                console.log("Impedance",Impedance[0].toString())

                                valueImpedance = Impedance[0].toString()
                                setValueImpedance(valueImpedance)
                                console.log("value impedance: ", valueImpedance)
                        })
                    });
                })
                .catch((error) => {
                    console.log(error);
                });
        }
        getApi()
    

    return (
        <View style={styles.container}>
            <Text>Test de la bio  impédance df</Text>
            <Text>Nom du peripherique: {Runsys.name}</Text>
            <Text> Hydratation: {lastvalueImpedance}  </Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    }
})

export default Page2;
