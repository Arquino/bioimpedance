import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Page2 from './src/screens/Page2'

export default function App() {
  return (
    <View style={styles.container}>
      
      <Page2 />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#00C9A3',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
